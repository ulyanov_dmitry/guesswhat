export const getBoxShadow = () => ({
  boxShadow: '2px 2px 12px 0px rgba(0,0,0,0.55)',
});

export const getSmallBoxShadow = () => ({
  boxShadow: '2px 2px 4px 1px rgba(0,0,0,0.55)',
});

export const createInfiniteAnimation = (keyframes, duration) => ({
  animationDuration: duration,
  animationFillMode: 'both',
  animationIterationCount: 'infinite',
  animationName: keyframes,
  animationTimingFunction: 'ease-in-out',
});