import { createStore, applyMiddleware, } from 'redux';
import createSaga from 'redux-saga';
import rootReducer from './reducer';
import rootSaga from './saga';

let store = null;
const sagaMiddleware = createSaga();

export const configureStore = () => {
  const middlewares = [];

  middlewares.push(sagaMiddleware);
  
  if (process.env.NODE_ENV === 'development') {
    const { createLogger } = require('redux-logger');
    middlewares.push(createLogger({
      collapsed: true,
      duration: true,
    }))
  }

  const enhancer = applyMiddleware(...middlewares);

  store = createStore(rootReducer, {}, enhancer);
  
  sagaMiddleware.run(rootSaga);
  
  return store;
};