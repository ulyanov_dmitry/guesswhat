import { createAction, handleActions } from 'redux-actions';
import { moduleName, gameStates } from './config';

const initialState = {
  dotsData: [],
  code: '',
  generatedCode: '',
  gameState: gameStates.NOT_STARTED,
};

export const INIT = `${moduleName}/INIT`;
export const init = createAction(INIT);

const SET_GAME_STATE = `${moduleName}/CHANGE_GAME_STATE`;
export const setGameState = createAction(SET_GAME_STATE);

const SET_GENERATED_CODE = `${moduleName}/SET_GENERATED_CODE`;
export const setGeneratedCode = createAction(SET_GENERATED_CODE);

const SET_USER_CODE = `${moduleName}/SET_USER_CODE`;
export const setUserCode = createAction(SET_USER_CODE);

const SET_DOTS_DATA = `${moduleName}/SET_DOTS_DATA`;
export const setDotsData = createAction(SET_DOTS_DATA);

export const USER_CODE_INPUT_CHANGED = `${moduleName}/USER_CODE_INPUT_CHANGED`;
export const userCodeInputChanged = createAction(USER_CODE_INPUT_CHANGED);

export const USER_TRIED_TO_GUESS = `${moduleName}/USER_TRIED_TO_GUESS`;
export const userTriedToGuess = createAction(USER_TRIED_TO_GUESS);

export const USER_GUESS_SUCCEED = `${moduleName}/USER_GUESS_SUCCEED`;
export const userGuessSucceed = createAction(USER_GUESS_SUCCEED);

export const USER_GUESS_FAILED = `${moduleName}/USER_GUESS_FAILED`;
export const userGuessFailed = createAction(USER_GUESS_FAILED);

export const REFRESH_GAME = `${moduleName}/REFRESH_GAME`;
export const refreshGame = createAction(REFRESH_GAME);

const CLEAR = `${moduleName}/CLEAR`;
export const clear = createAction(CLEAR);

export default handleActions({
  
  [SET_GAME_STATE]: (state, { payload }) => ({
    ...state,
    gameState: Object.values(gameStates).includes(payload) ? payload : gameStates.NOT_STARTED,
  }),
  
  [SET_GENERATED_CODE]: (state, { payload }) => ({
    ...state,
    generatedCode: payload,
  }),
  
  [SET_USER_CODE]: (state, { payload }) => ({
    ...state,
    code: payload,
  }),
  
  [SET_DOTS_DATA]: (state, { payload }) => ({
    ...state,
    dotsData: payload,
  }),
  
  [CLEAR]: (state, action) => ({
    ...initialState,
  })
  
}, initialState);