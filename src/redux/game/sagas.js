import { delay, } from 'redux-saga';
import { take, put, call, fork, cancel, select } from 'redux-saga/effects';

import * as gameActions from './';
import { gameStates } from './config';
import * as gameSelectors from './selectors';

import { generateCode } from '../../utils/code-generator';
import { getDigits } from '../../utils/get-digits';

function * game() {
  yield take(gameActions.INIT);
  while (true) {
    try {
      yield put(gameActions.setGameState(gameStates.LOADING));
      yield call(gameInitialization);
  
      yield put(gameActions.setGameState(gameStates.IN_PROGRESS));
      yield call(gameplay);
  
      yield put(gameActions.setGameState(gameStates.END));
      yield take(gameActions.REFRESH_GAME);
      yield put(gameActions.clear());
    } catch (error) {
      console.log('Some unexpected error =(, %O', error);
      yield put(gameActions.clear());
    }
  }
}

function * gameInitialization() {
  const code = yield call(generateCode, 4);
  console.log(`%cHere is the right code, trust me: ${code}`, 'font-weight: bold; color: #ad4885');
  yield put(gameActions.setDotsData(new Array(code.length).fill(false)));
  yield delay(1200); // DELETE delay for loader only
  yield put(gameActions.setGeneratedCode(code));
}

function * gameplay() {
  const [ tries, inputs ] = yield [
    fork(watchTries),
    fork(watchCodeInputs)
  ];
  
  yield take(gameActions.USER_GUESS_SUCCEED);
  
  yield [
    cancel(tries),
    cancel(inputs),
  ];
}

function * watchTries() {
  while (true) {
    yield take(gameActions.USER_TRIED_TO_GUESS);
    
    const { code, generatedCode } = yield select(state => ({
      code: gameSelectors.getUserCode(state),
      generatedCode: gameSelectors.getGeneratedCode(state),
    }));
    
    yield put(gameActions.setDotsData(getDotsData(code, generatedCode)))
    
    if (code === generatedCode) {
      yield delay(400);
      yield put(gameActions.userGuessSucceed());
    } else {
      yield put(gameActions.userGuessFailed());
    }
  }
}

function * watchCodeInputs() {
  while (true) {
    const { payload } = yield take(gameActions.USER_CODE_INPUT_CHANGED);
    const code = getDigits(payload);
    yield put(gameActions.setUserCode(code));
  }
}

function getDotsData(code, generatedCode) {
  const userCodeArray = code.split('');
  const generatedCodeArray = generatedCode.split('');
  return [ ...new Array(generatedCode.length) ].map((v, i) => {
    return userCodeArray[ i ] === generatedCodeArray[ i ];
  });
}

export default [
  game,
];