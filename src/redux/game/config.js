export const moduleName = 'game';

export const gameStates = {
  NOT_STARTED: 'not started',
  LOADING: 'loading',
  IN_PROGRESS: 'started, in progress',
  END: 'game ended',
};