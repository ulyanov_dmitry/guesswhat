import { moduleName } from './config';

export const getUserCode = state => state[ moduleName ].code;
export const getGeneratedCode = state => state[ moduleName ].generatedCode;
export const getCodeLength = state => state[ moduleName ].generatedCode.length;
export const getGameState = state => state[ moduleName ].gameState;
export const getDotsData = state => state[ moduleName ].dotsData;