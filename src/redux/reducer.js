import { combineReducers } from 'redux';
import game from './game';
import { moduleName as gameModuleName } from './game/config';

export default combineReducers({
  [gameModuleName]: game,
})