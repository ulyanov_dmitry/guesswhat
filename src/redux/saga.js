import { fork } from 'redux-saga/effects';
import gameSagas from './game/sagas';

const forker = saga => fork(saga);

export default function * rootSaga() {
  yield [
    ...gameSagas
  ].map(forker);
};