export const getDigits = value => value
  .split('')
  .filter(char => Number.isInteger(+char))
  .join('');