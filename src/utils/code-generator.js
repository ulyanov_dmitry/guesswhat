const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

const getSingleDigitNumber = () => getRandomInt(0, 9);

export const generateCode = (length = 4) => {
  return [...new Array(Math.max(1, length))].map(getSingleDigitNumber).join('');
};

