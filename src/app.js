import React from 'react';
import { Provider } from 'react-redux';
import { configureStore } from './redux/store';
import Game from './modules/game';

const store = configureStore();

const App = () => {
  return (
    <Provider store={store}>
      <Game />
    </Provider>
  );
};

export default App;