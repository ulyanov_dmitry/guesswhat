import { StyleSheet } from 'aphrodite';
import { getBoxShadow } from '../../styles';

export default StyleSheet.create({
  dots: {
    boxSizing: 'border-box',
    width: '90%',
    padding: 14,
    
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  dot: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    boxSizing: 'border-box',
    
    width: 44,
    height: 44,
    borderRadius: '50%',
    backgroundColor: 'white',
    transitionDuration: '0.4s',
  },
  filled: {
    width: 48,
    height: 48,
    color: '#F06292',
    backgroundColor: 'white',
    ...getBoxShadow(),
  },
});