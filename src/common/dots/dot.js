import React from 'react';
import { css } from 'aphrodite';
import styles from './styles';

import IconSuccess from 'react-icons/lib/md/check';
import IconFailure from 'react-icons/lib/go/x';

const Dot = ({
    filled,
    ...props
  }) => {
  const style = [
    styles.dot,
    filled && styles.filled,
  ];
  
  return (
    <div className={css(style)}>
      {
        filled ?
          <IconSuccess height="1.5em" width="1.5em" /> :
          <IconFailure height="1.3em" width="1.3em" />
      }
    </div>
  );
};

export default Dot;