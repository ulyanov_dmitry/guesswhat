import React from 'react';
import { css } from 'aphrodite';
import styles from './styles';

import Dot from './dot';

const renderDot = (filled, key) => <Dot key={key} filled={filled}/>;

const Dots = ({
    data,
    ...props
  }) => {
  return (
    <div className={css(styles.dots)}>
      {data.map(renderDot)}
    </div>
  );
};

Dots.propTypes = {
  data: React.PropTypes.array.isRequired,
};

export default Dots;