import { StyleSheet } from 'aphrodite';
import { getBoxShadow, getSmallBoxShadow } from '../../styles';

const length = 90;

export default StyleSheet.create({
  button: {
    boxSizing: 'border-box',
    outline: 'none',
    border: 0,
    padding: 14,
    borderRadius: length / 2,
    width: length,
    height: length,
    
    fontSize: 24,
    color: 'white',
    backgroundColor: '#F06292',
    ...getBoxShadow(),
    
    transitionDuration: '0.2s',
    
    ':hover': {
      backgroundColor: '#EC407A',
    },
    
    ':active': {
      backgroundColor: '#E91E63',
      ...getSmallBoxShadow(),
    }
  }
});