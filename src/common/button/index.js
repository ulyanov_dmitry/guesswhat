import React from 'react';
import { css } from 'aphrodite';
import styles from './styles';

import RefreshIcon from 'react-icons/lib/md/refresh';
import SendIcon from 'react-icons/lib/md/send';

const Button = ({
    children,
    onClick,
    ...props
  }) => {
  const style = [
    styles.button,
  ];
  
  return (
    <button className={css(style)} onClick={onClick}>
      {children}
    </button>
  );
};

Button.propTypes = {
  onClick: React.PropTypes.func.isRequired,
};

export const RefreshButton = (props) => (
  <Button {...props}><RefreshIcon height="1.8em" width="1.8em" /></Button>
);

export const CheckButton = (props) => (
  <Button {...props}><SendIcon height="1.8em" width="1.8em" /></Button>
);

export default Button;