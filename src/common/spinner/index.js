import React from 'react';
import { css } from 'aphrodite';
import styles from './styles';

const Spinner = () => (
  <div className={css(styles.container)}>
    <div className={css(styles.bounce,)}/>
    <div className={css(styles.bounce, styles.delay)}/>
    <div className={css(styles.bounce, styles.doubleDelay)}/>
  </div>
);

export default Spinner;