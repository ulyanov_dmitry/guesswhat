import { StyleSheet } from 'aphrodite';
import { createInfiniteAnimation } from '../../styles';

const bounceKeyframes = {
  '0%': { transform: 'scale(0.0)' },
  '40%': { transform: 'scale(1.0)' },
  '80%': { transform: 'scale(0.0)' },
  '100%': { transform: 'scale(0.0)' },
};

const bounceSize = 18;
const margin = 8;

export default StyleSheet.create({
  container: {
    width: bounceSize * 3 + margin * 2 + 10,
    height: bounceSize,
    margin: 'auto',
    position: 'relative',
    alignText: 'center',
  },
  bounce: {
    width: bounceSize,
    height: bounceSize,
    borderRadius: '50%',
    display: 'inline-block',
    marginRight: margin,
    
    backgroundColor: 'white',
    
    ...createInfiniteAnimation(bounceKeyframes, '1.4s'),
  },
  
  delay: {
    animationDelay: '0.16s',
  },
  
  doubleDelay: {
    animationDelay: '0.32s',
  },
  
})