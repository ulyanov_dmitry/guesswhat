import React from 'react';
import { css } from 'aphrodite';
import styles from './styles';

const Heading = ({
    children,
    ...props
  }) => {
  const style = [
    styles.heading,
  ];
  
  return (
    <span className={css(style)}>
      {children}
    </span>
  );
};

export default Heading;