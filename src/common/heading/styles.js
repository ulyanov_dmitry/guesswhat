import { StyleSheet } from 'aphrodite';

export default StyleSheet.create({
  heading: {
    fontSize: 36,
    boxSizing: 'border-box',
    margin: '0 20px 20px 20px',
    color: 'white',
    fontWeight: 'bold',
  }
})