import React from 'react';
import { css } from 'aphrodite';
import styles from './styles';

const Input = ({
    onChange,
    value,
    bold,
    ...props
  }) => {
  const style = [
    styles.input,
    bold && styles.bold,
  ];
  
  return (
    <input
      value={value}
      onChange={onChange}
      className={css(style)}
      {...props}
    />
  );
};

Input.propTypes = {
  value: React.PropTypes.string.isRequired,
  onChange: React.PropTypes.func.isRequired,
  bold: React.PropTypes.bool,
};

export default Input;