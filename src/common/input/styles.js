import { StyleSheet } from 'aphrodite';
import { getBoxShadow } from '../../styles';

export default StyleSheet.create({
  input: {
    border: 0,
    padding: 14,
    margin: '0 20px 20px 0',
    borderRadius: 4,
    outline: 'none',
    fontSize: 24,
    textAlign: 'center',
    ':focus': {
      ...getBoxShadow(),
    },
    transitionDuration: '0.33s',
  },
  bold: {
    fontWeight: 'bold',
  },
});