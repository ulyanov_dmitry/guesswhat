import React from 'react';
import { connect } from 'react-redux';

import * as gameActions from '../../redux/game/';
import * as gameSelectors from '../../redux/game/selectors';
import * as gameConfig from '../../redux/game/config';

import Layout from './layout';
import Dots from '../../common/dots';
import Input from '../../common/input';
import Heading from '../../common/heading';
import Spinner from '../../common/spinner';
import { RefreshButton, CheckButton } from '../../common/button';

const mapStateToProps = state => {
  return ({
    code: gameSelectors.getUserCode(state),
    codeLength: gameSelectors.getCodeLength(state),
    gameState: gameSelectors.getGameState(state),
    dotsData: gameSelectors.getDotsData(state),
  });
};

const mapDispatchToProps = dispatch => ({
  initGame: () => dispatch(gameActions.init()),
  onCodeChange: (e) => dispatch(gameActions.userCodeInputChanged(e.target.value)),
  onTryClick: () => dispatch(gameActions.userTriedToGuess()),
  onEnterPress: (e) => e.key === 'Enter' && dispatch(gameActions.userTriedToGuess()),
  onRefreshClick: () => dispatch(gameActions.refreshGame()),
});

class Game extends React.Component {
  
  componentDidMount() {
    this.props.initGame();
  }
  
  render() {
    const {
      code,
      codeLength,
      gameState,
      dotsData,
      onTryClick,
      onEnterPress,
      onRefreshClick,
      onCodeChange,
    } = this.props;
    
    if (gameState === gameConfig.gameStates.NOT_STARTED) {
      return <Layout />;
    }
    
    if (gameState === gameConfig.gameStates.LOADING) {
      return (
        <Layout>
          <Spinner />
        </Layout>
      );
    }
    
    if (gameState === gameConfig.gameStates.END) {
      return (
        <Layout>
          <Heading>Wow! You guessed!</Heading>
          <RefreshButton onClick={onRefreshClick} />
        </Layout>
      );
    }
    
    return (
      <Layout>
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <Heading>Guess the code, dude!</Heading>
          <div style={{ justifyContent: 'space-around' }}>
            <Input bold
                   placeholder={`Enter ${codeLength} digits`}
                   value={code}
                   maxLength={codeLength}
                   onChange={onCodeChange}
                   onKeyPress={onEnterPress}
            />
            <CheckButton onClick={onTryClick} />
          </div>
        </div>
        <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <Heading>Your results is here</Heading>
          <Dots data={dotsData}/>
        </div>
      </Layout>
    );
  }
  
}

export default connect(mapStateToProps, mapDispatchToProps)(Game);