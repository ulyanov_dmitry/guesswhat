import React from 'react';
import { css } from 'aphrodite';
import styles from './styles';

export default (props) => (
  <div className={css(styles.layout)}>
    {React.Children.map(props.children, child => child)}
  </div>
);