import { StyleSheet } from 'aphrodite';
import { getBoxShadow } from '../../styles';

export default StyleSheet.create({
  layout: {
    boxSizing: 'border-box',
    margin: 0,
    padding: 24,
    fontFamily: 'sans-serif',
    
    ...getBoxShadow(),
    backgroundColor: '#009688',
    borderRadius: '20px',
    height: '80vh',
    width: '80vw',
    maxWidth: 640,
  
    display: 'flex',
    flexDirection: 'column',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-around',
  }
});